class Post < ApplicationRecord

  has_many :comments

  validates :name, presence: true, length: {minimum: 5 }
  validates :title, presence: true, length: {maximum: 30}
end
